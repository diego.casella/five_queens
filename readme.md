Puzzle:
> Suppose you have a 5x5 playing field. Place 5 white queens (chess rules) and 3 black queens so that none of the
> white ones can attack a black one (and vice versa).

How to run the project

start sbt from the project root directory, compile the project then run it

```
five_queens$: sbt
sbt:five_queens> compile
....
sbt:five_queens> run
processed 8 solutions in 145 ms
...
```
