package org.fivequeens

import java.lang.System.nanoTime
import java.util.concurrent.TimeUnit

object Main {
  private val MAX_ROWS = 5
  private val MAX_COLS = 5
  private val WHITE_QUEENS_NR = 5
  private val BLACK_QUEENS_NR = 3

  case class Queen(x: Int, y: Int) {

    def attacksCell(x1: Int, y1: Int): Boolean = x == x1 || // same column
        y == y1 || // same row
        x - y == x1 - y1 || // same diagonal ( x = y )
        x + y == x1 + y1 // same inverted diagonal ( x = -y )

  }

  def main(args: Array[String]): Unit = {
    val start = nanoTime()

    // create a board full of queens
    val fullBoard = for {
      i <- 0 until MAX_ROWS
      j <- 0 until MAX_COLS
    } yield Queen(i, j)

    // extract all possible combinations of white queens
    val solutions = fullBoard.combinations(WHITE_QUEENS_NR)
      .flatMap(whiteQueens => {
        // take out all the cells that can be attacked by the white queens; the remaining ones can be occupied by black queens
        whiteQueens.foldLeft(fullBoard)((board, queen) =>
          board.filterNot(cell => queen.attacksCell(cell.x, cell.y))) match {
          case blackQueens if blackQueens.size == BLACK_QUEENS_NR => Some((whiteQueens, blackQueens))
          case _ => None
        }
      })
      .toList

    val end = nanoTime()
    val ms = TimeUnit.MILLISECONDS.convert(end - start, TimeUnit.NANOSECONDS)

    println(s"processed ${solutions.size} solutions in $ms ms")

    solutions.zipWithIndex.foreach { case ((wq, bq), index) => print(wq, bq, index)}
  }

  private def print(whites: Seq[Queen], blacks: Seq[Queen], index: Int) = {
    println(s"Solution $index")
    println("____" * MAX_ROWS)
    0 until MAX_ROWS foreach { r =>
      val theRow = 0 until MAX_COLS map { c =>
        whites.find(w => w.x == c && w.y == r) match {
          case Some(_) => "W"
          case None => blacks.find(b => b.x == c && b.y == r) match {
            case Some(_) => "B"
            case None    => " "
          }
        }
      }

      println(theRow.mkString("| ", " | ", " |"))
    }
    println("____" * MAX_ROWS)
    println("")
    println("")
  }
}
